from ..base import BaseApiModel

class EmailType(BaseApiModel):
    def __init__(self, id=None):
        super(EmailType, self).__init__(id=id)

        self._id = id

    class _meta:
        pk_name = 'id'
        verbose_name = 'email type'
        verbose_name_plural = 'email types'
        url = '/actions/mail/types'

        attribute_types = {
            'id': {'type': 'str', 'required': False}
        }

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, id):
        self._id = id
