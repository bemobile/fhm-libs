from ..exceptions import APIError
from ..query import ActionQuery, InsertQuery, SelectQuery
from ..base import BaseApiModel


class PromoAction(BaseApiModel):
    def __init__(self, id=None, code_name=None, amount=None, amount_type=None, applies_on=None, shows_on=None,
                 description=None, images=None, status=None, constraints=None, times_can_be_applied=None,
                 time_counter_days=None, time_counter_field=None, campaign=None):
        super(PromoAction, self).__init__(id=id, code_name=code_name, amount=amount, amount_type=amount_type,
                                          applies_on=applies_on, shows_on=shows_on, status=status,
                                          images=images, constraints=constraints, description=description,
                                          times_can_be_applied=times_can_be_applied, campaign=campaign,
                                          time_counter_days=time_counter_days, time_counter_field=time_counter_field)
        self._id = id
        self._code_name = code_name
        self._amount = amount
        self._amount_type = amount_type
        self._applies_on = applies_on
        self._shows_on = shows_on
        self._images = images
        self._status = status
        self._constraints = constraints
        self._times_can_be_applied = times_can_be_applied
        self._campaign = campaign
        self._description = description
        self._time_counter_days = time_counter_days
        self._time_counter_field = time_counter_field

    class _meta:
        pk_name = 'id'
        verbose_name = 'promo'
        verbose_name_plural = 'promos'
        url = {
            SelectQuery.identifier: '/campaigns/%(campaign)s/actions/promo',
            InsertQuery.identifier: '/campaigns/%(campaign)s/actions/promo',
            'VALIDATE_FOR_USER': '/users/%(user_id)s/actions/promo/%(code_name)s/validate',
        }
        attribute_types = {
            'id': {'type': 'str', 'required': False},
            'code_name': {'type': 'str', 'required': True},
            'amount': {'type': 'float', 'required': True},
            'amount_type': {'type': 'str', 'required': True},
            'status': {'type': 'str', 'required': False},
            'applies_on': {'type': 'str', 'required': True},
            'shows_on': {'type': 'list', 'required': True},
            'images': {'type': 'Images', 'required': False},
            'constraints': {'type': 'list', 'required': False},
            'times_can_be_applied': {'type': 'int', 'required': False},
            'campaign': {'type': 'Campaign', 'required': True},
            'description': {'type': 'dict', 'required': False},
            'time_counter_days': {'type': 'int', 'required': False},
            'time_counter_field': {'type': 'str', 'required': True},
        }

    def validate(self, user, product_properties):
        action = ActionQuery(
            PromoAction,
            self.code_name,
            'VALIDATE_FOR_USER',
            params={'user_id': user.id, 'code_name': self.code_name},
            product=product_properties
        )
        return action.execute()

    @classmethod
    def create_mgm_promo(cls, code_name, campaign, user):
        action = InsertQuery(
            PromoAction,
            code_name=code_name,
            campaign=campaign.id,
            constraints=[{'attribute': 'user.id', 'operator': '!=', 'value': user.id},
                         {"attribute": "total_price.amount", "operator": ">=", "value": "20"},
                         {"attribute": "user.has_bought", "operator": "==", "value": "false"}]
        )
        result = action.execute()
        obj = cls()
        for key, value in result.items():
            setattr(obj, key, value)
        return obj

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, id):
        self._id = id

    @property
    def code_name(self):
        return self._code_name

    @code_name.setter
    def code_name(self, code_name):
        self._code_name = code_name

    @property
    def amount(self):
        return self._amount

    @amount.setter
    def amount(self, amount):
        self._amount = amount

    @property
    def amount_type(self):
        return self._amount_type

    @amount_type.setter
    def amount_type(self, amount_type):
        self._amount_type = amount_type

    @property
    def applies_on(self):
        return self._applies_on

    @applies_on.setter
    def applies_on(self, applies_on):
        self._applies_on = applies_on

    @property
    def shows_on(self):
        return self._shows_on

    @shows_on.setter
    def shows_on(self, shows_on):
        self._shows_on = shows_on

    @property
    def images(self):
        return self._images

    @images.setter
    def images(self, images):
        self._images = images

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        self._status = status

    @property
    def constraints(self):
        return self._constraints

    @constraints.setter
    def constraints(self, constraints):
        self._constraints = constraints

    @property
    def times_can_be_applied(self):
        return self._times_can_be_applied

    @times_can_be_applied.setter
    def times_can_be_applied(self, times_can_be_applied):
        self._times_can_be_applied = times_can_be_applied

    @property
    def campaign(self):
        return self._campaign

    @campaign.setter
    def campaign(self, campaign):
        self._campaign = campaign

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, description):
        self._description = description

    @property
    def time_counter_days(self):
        return self._time_counter_days

    @time_counter_days.setter
    def time_counter_days(self, time_counter_days):
        self._time_counter_days = time_counter_days

    @property
    def time_counter_field(self):
        return self._time_counter_field

    @time_counter_field.setter
    def time_counter_field(self, time_counter_field):
        self._time_counter_field = time_counter_field

