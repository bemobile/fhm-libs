from ..base import BaseApiModel
from ..query import ActionQuery, InsertQuery, SelectQuery, UpdateQuery


class User(BaseApiModel):
    def __init__(self, id=None, email=None, first_name=None, **kwargs):
        super(User, self).__init__(id=id, email=email, first_name=first_name)

        self._id = id
        self._email = email
        self._first_name = first_name
        self._properties = kwargs

    class _meta:
        pk_name = 'id'
        verbose_name = 'user'
        verbose_name_plural = 'users'
        url = {
            SelectQuery.identifier: '/users',
            InsertQuery.identifier: '/users',
            UpdateQuery.identifier: '/users',
            'ACTIVATE_EMAIL': '/users/%(id)s/activate-email',
            'DEACTIVATE_EMAIL': '/users/%(id)s/deactivate-email',
        }

        attribute_types = {
            'id': {'type': 'str', 'required': False},
            'email': {'type': 'str', 'required': True},
            'first_name': {'type': 'str', 'required': True}
        }

    def activate_email(self, mail_type):
        action = ActionQuery(
            User,
            self.id,
            'ACTIVATE_EMAIL',
            mail_type=mail_type
        )
        return action.execute()

    def deactivate_email(self, mail_type):
        action = ActionQuery(
            User,
            self.id,
            'DEACTIVATE_EMAIL',
            mail_type=mail_type
        )
        return action.execute()

    def as_dict(self):
        result = super(User, self).as_dict()
        for k, v in self._properties.iteritems():
            if k not in result:
                result[k] = v
        return result

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, id):
        self._id = id

    @property
    def email(self):
        return self._email

    @email.setter
    def email(self, email):
        self._email = email

    @property
    def first_name(self):
        return self._first_name

    @first_name.setter
    def first_name(self, first_name):
        self._first_name = first_name

    @property
    def properties(self):
        return self._properties

    @properties.setter
    def properties(self, properties):
        self._properties = properties
