from exceptions import APIError


class Config:
    base_url = ''
    client_id = ''
    client_passphrase = ''
    token = ''


class Crm(object):
    def __init__(self):
        self.config = Config

    def init_app(self, app):
        self.config.client_id = app.config['CRM_SETTINGS']['CLIENT_ID']
        self.config.client_passphrase = app.config['CRM_SETTINGS']['CLIENT_PASSPHRASE']
        self.config.base_url = app.config['CRM_SETTINGS']['HOST']
        self.config.token = get_token()


def get_token():
    try:
        from api import APIRequest
        _, response = APIRequest.init_app().request('POST', '/token/'+ Config.client_id,
                                                    data={'passphrase': Config.client_passphrase})
        return response['data']['token']
    except APIError as e:
        return None